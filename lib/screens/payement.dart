import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_application_likeevent/models/movie.dart';
import 'package:flutter_credit_card/flutter_credit_card.dart';
import 'package:flutter_credit_card/credit_card_brand.dart';
import 'package:flutter_credit_card/credit_card_form.dart';
import 'package:flutter_credit_card/credit_card_model.dart';



class Payement extends StatefulWidget {
  @override
  _PayementState createState() => _PayementState();
}

class _PayementState extends State<Payement> {
  String cardNumber = '';
  String expiryDate = '';
  String cardHolderName = '';
  String cvvCode = '';
  bool isCvvFocused = false;
  bool useGlassMorphism = false;
  bool useBackgroundImage = true;
  OutlineInputBorder? border;
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    MediaQueryData mediaQuery = MediaQuery.of(context);
    var screenSize = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'PAYEMENT',
          style: TextStyle(
            color: Colors.purple,
            letterSpacing: 1.0,
            fontSize: 20.0,
          ), //textstyle
        ), //TEXT
        centerTitle: true,
        backgroundColor: Colors.white,
        leading: GestureDetector(
          onTap: () {
            /* Write listener code here */
          },
          child: Icon(
            Icons.arrow_back_outlined, // add custom icons also
            color: Colors.purple,
          ), //icon
        ), //gesture
      ), //AppBar
      body: SingleChildScrollView(
        child: Center(
          child: Container(
            height: MediaQuery.of(context).size.height*1.4,
            decoration: BoxDecoration(
              gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: [
                  Colors.purple,
                  Colors.deepPurple,
                  Colors.purple,
                  Colors.deepPurple,
                ],
              ),
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                ListView.builder(
                  shrinkWrap: true,
                  physics: NeverScrollableScrollPhysics(),
                  itemCount: movieList12.length,
                  itemBuilder: (BuildContext context, index) =>
                      movieTile12(context, movie: movieList12[index]),
                ),
                const SizedBox(height: 20,),
                Container(
                  padding: EdgeInsets.only(bottom: mediaQuery.viewInsets.bottom),
                  height: screenSize.height,
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.only(
                        topLeft: const Radius.circular(40.0),
                        topRight: const Radius.circular(40.0),
                      )),
                  child: SafeArea(
                    child: Column(
                      children: <Widget>[
                        const SizedBox(
                          height: 30,
                        ),
                        CreditCardWidget(
                          cardNumber: cardNumber,
                          expiryDate: expiryDate,
                          cardHolderName: cardHolderName,
                          cvvCode: cvvCode,
                          showBackView: isCvvFocused,
                          obscureCardNumber: true,
                          obscureCardCvv: true,
                          isHolderNameVisible: true,
                          cardBgColor: Colors.red,
                          backgroundImage: useBackgroundImage ? 'assets/card_bg.png' : null,
                          isSwipeGestureEnabled: true,
                          onCreditCardWidgetChange: (CreditCardBrand creditCardBrand) {},
                          customCardTypeIcons: <CustomCardTypeIcon>[
                            CustomCardTypeIcon(
                              cardType: CardType.mastercard,
                              cardImage: Image.asset(
                                'assets/images/mastercard.jpg',
                                height: 48,
                                width: 48,
                              ),
                            ),
                          ],
                        ),
                        Expanded(
                          child: SingleChildScrollView(
                            reverse: true,
                            child: Column(
                              children: <Widget>[
                                CreditCardForm(
                                  formKey: formKey,
                                  obscureCvv: true,
                                  obscureNumber: true,
                                  cardNumber: cardNumber,
                                  cvvCode: cvvCode,
                                  isHolderNameVisible: true,
                                  isCardNumberVisible: true,
                                  isExpiryDateVisible: true,
                                  cardHolderName: cardHolderName,
                                  expiryDate: expiryDate,
                                  themeColor: Colors.blue,
                                  textColor: Colors.black,
                                  cardNumberDecoration: InputDecoration(
                                    labelText: 'Card Number',
                                    hintText: 'XXXX XXXX XXXX XXXX',
                                    hintStyle: const TextStyle(
                                        color: Colors.grey, fontFamily: 'Poppins'),
                                    labelStyle: const TextStyle(
                                        color: Colors.purple, fontFamily: 'Poppins'),
                                    focusedBorder: border,
                                    enabledBorder: border,
                                  ),
                                  expiryDateDecoration: InputDecoration(
                                    hintStyle: const TextStyle(
                                        color: Colors.black, fontFamily: 'Poppins'),
                                    labelStyle: const TextStyle(
                                        color: Colors.purple, fontFamily: 'Poppins'),
                                    focusedBorder: border,
                                    enabledBorder: border,
                                    labelText: 'Expire Date',
                                    hintText: 'XX/XX',
                                  ),
                                  cvvCodeDecoration: InputDecoration(
                                    hintStyle: const TextStyle(
                                        color: Colors.black, fontFamily: 'Poppins'),
                                    labelStyle: const TextStyle(
                                        color: Colors.purple, fontFamily: 'Poppins'),
                                    focusedBorder: border,
                                    enabledBorder: border,
                                    labelText: 'CVV',
                                    hintText: 'XXX',
                                  ),
                                  cardHolderDecoration: InputDecoration(
                                    hintStyle: const TextStyle(
                                        color: Colors.black, fontFamily: 'Poppins'),
                                    labelStyle: const TextStyle(
                                        color: Colors.purple, fontFamily: 'Poppins'),
                                    focusedBorder: border,
                                    enabledBorder: border,
                                    labelText: 'Card Holder',
                                  ),

                                  onCreditCardModelChange: onCreditCardModelChange,
                                ),
                                const SizedBox(
                                  height: 20,
                                ),
                                const SizedBox(
                                  height: 20,
                                ),


                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                const SizedBox(
                  height: 10,
                ),
                Container(
                  margin: EdgeInsets.symmetric(horizontal:MediaQuery.of(context).size.width * 0.14),
                  child:SizedBox(
                    height:  MediaQuery.of(context).size.width * 0.13,
                    width: MediaQuery.of(context).size.width * 0.75,
                    child: ElevatedButton(
                      onPressed: (){},
                      child: const Text('Pay secure',),
                      style: ElevatedButton.styleFrom(
                        primary: Colors.white,
                        onPrimary: Colors.purple,
                        onSurface: Colors.grey,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Container movieTile12(BuildContext context, {required Movie movie}) {
    return Container(
      height: MediaQuery.of(context).size.width * 0.4,
      decoration: BoxDecoration(
        color: Colors.white,
      ), //box
      padding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 2.0),
      child: Row(

        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                margin: EdgeInsets.symmetric(horizontal: 8.0, vertical: 1.0),
                clipBehavior: Clip.antiAlias,
                height: MediaQuery.of(context).size.width*0.5,
                width: MediaQuery.of(context).size.width*0.3,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20.0),
                  image: DecorationImage(
                    image: AssetImage('assets/images/loup.png'),
                    fit: BoxFit.fill,
                  ),
                ),
              ),
              SizedBox(width: MediaQuery.of(context).size.width*0.1),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Le loup',
                    style: TextStyle(
                      color: Colors.purple,
                      fontSize: 20.0,
                    ),
                  ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      SizedBox(
                          height: MediaQuery.of(context).size.width * 0.009),
                      Container(
                        width: MediaQuery.of(context).size.width * 0.40,
                        child: Text(
                          'DAY : 20/09/2021',
                          style: TextStyle(
                            color: Colors.purple,
                            fontSize: 18.0,
                          ), //textstyle
                        ),
                      ),
                      SizedBox(
                          height: MediaQuery.of(context).size.width * 0.009),
                      Container(
                        width: MediaQuery.of(context).size.width * 0.40,
                        child: Text(
                          'PLACE : PATHE',
                          style: TextStyle(
                            color: Colors.purple,
                            fontSize: 18.0,
                          ), //textstyle
                        ),
                      ),
                      SizedBox(
                          height: MediaQuery.of(context).size.width * 0.009),
                      Container(
                        width: MediaQuery.of(context).size.width * 0.40,
                        child: Text(
                          'DATE : 18:00',
                          style: TextStyle(
                            color: Colors.purple,
                            fontSize: 18.0,
                          ), //textstyle
                        ),
                      ),
                      SizedBox(
                          height: MediaQuery.of(context).size.width * 0.009),
                      Container(
                        width: MediaQuery.of(context).size.width * 0.40,
                        child: Text(
                          'PRIX : 16.00DT',
                          style: TextStyle(
                            color: Colors.purple,
                            fontSize: 18.0,
                          ), //textstyle
                        ),
                      ),

                    ],
                  ),
                ],
              ),
            ],
          ),
        ],
      ),
    );
  }

  void onCreditCardModelChange(CreditCardModel? creditCardModel) {
    setState(() {
      cardNumber = creditCardModel!.cardNumber;
      expiryDate = creditCardModel.expiryDate;
      cardHolderName = creditCardModel.cardHolderName;
      cvvCode = creditCardModel.cvvCode;
      isCvvFocused = creditCardModel.isCvvFocused;
    });
  }
}
