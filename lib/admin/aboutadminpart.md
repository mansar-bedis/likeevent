There were issues implementing the admin pages in the application since we couldn't implement a system
that separates a regular user from an admin and the different features that comes with that difference.
So we did the admin pages separately in the admin directory without featuring them in our application.